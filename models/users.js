"use strict";
const { Model } = require("sequelize");
const { encode } = require("../helpers/bcrypt");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User.init(
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "email Tidak Boleh Kosong",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "Password Tidak Boleh Kosong",
          },
        },
      },
      role: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            msg: "Role Number Tidak Boleh Kosong",
          },
        },
      },
    },
    {
      hooks: {
        beforeCreate(user) {
          user.password = encode(user.password);
        },
      },
      sequelize,
      modelName: "User",
      tableName: "users",
    }
  );
  return User;
};
